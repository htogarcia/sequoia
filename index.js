
/* Required packages for node JS */
var express = require('express');
var path = require('path');
var fs = require('fs');
var bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient

/* Connection to the database */
var db;
const auth = require('./auth');
var app = express();
app.use(auth);

var http = require('http').Server(app);

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

/* declare public the content of these directories */
app.use(express.static(__dirname + '/public/css'));
app.use(express.static(__dirname + '/public/js'));
app.use(express.static(__dirname + '/public/html'));
app.use('/files', express.static('files'));
app.use('/images', express.static('images'));

app.get('/', function (req, res) {
  res.sendFile('index.html');
});

app.get('/test', function (req, res) {
  res.sendFile(path.join(__dirname + '/public/html/login.html'));
});

app.get('/alarmas', function (req, res) {
  res.sendFile(path.join(__dirname + '/public/html/alarmas.html'));
});

app.get('/cambio', function (req, res) {
  res.sendFile(path.join(__dirname + '/public/html/cambio.html'));
});

/**
 * [API method] returns files' names inside 'files' directory
 * endpoint: hostname/dendrometros
 */
app.get('/dendrometros', function (req, res) {
  var filenames = [];
  fs.readdir('files/', function (err, files) {
    files.forEach(file => {
      if (file.includes("Mediciones")) {
        var dendometer_name = file.replace('Mediciones_','').replace('.txt','');
        var file_info = {name: file, dendrometer: dendometer_name};
        filenames.push(file_info);
      }
    });
    res.json(filenames);
  });
});

/**
 * [API method] writes in the database a reset petition for a given dendrometer
 * endpoint: hostname/reset
 */
app.post('/reset', (req, res) => {
  db.collection('reset').save(req.body, (err, result) => {
    if (err) return console.log(err)
    console.log('reset ' + req.body._id + ' saved to database')
    res.send('yes')
  })
})

/**
 * [API method] writes in the database a new alarm for a given dendrometer
 * endpoint: hostname/set
 */
app.post('/set', (req, res) => {
  db.collection('set').update({_id:req.body._id},req.body, {upsert: true}, (err, result) => {
    if (err) return console.log(err)
    console.log('alarm ' + req.body._id + ' saved to database')
    res.send('yes')
  })
})

var port = process.env.PORT || 3000;

/******************************************************************************/




/******************************************************************************/

/* Database connection */
MongoClient.connect('mongodb://sequoia:root@ds147668.mlab.com:47668/sequoia', (err, client) => {
  if (err) return console.log(err)
  db = client.db('sequoia') // whatever your database name is

  http.listen(port, function(){
    console.log('listening on http://localhost:' + port);
  });

})
