#!/bin/bash

##########################
# Variables              #
##########################

INSTALL='sudo install --owner=root --group=root --mode=644'


##########################
# Check permissions      #
##########################

# Check for permissions errors
if [ `id -u` == 0 ]; then
    echo "[ERROR] This script should not be executed as root. Run it a a sudo-capable user."
    exit 1
fi

# Check if user can do sudo
echo "Se necesitan permisos de administrador"
if [ `sudo id -u` != 0 ]; then
    echo "This user cannot cast sudo or you typed an incorrect password (several times)."
    exit 1
else
    echo "Correctly authenticated."
fi


# may the force be with you from here

python -m pip install pymongo

wget https://nodejs.org/dist/v9.11.1/node-v9.11.1-linux-armv6l.tar.gz
tar -xvf node-v9.11.1-linux-armv6l.tar.gz
cd node-v9.11.1-linux-armv6l/
sudo cp -R * /usr/local/

npm install -g openode
