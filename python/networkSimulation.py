'''
This script generates random data to emulate the dendrometers network
@author: Humberto Barrantes
@date: Apr-2018
'''

'''
Imports
'''
from random import randint
import time
import os

'''
First section, get how often each dendrometer will be taking measures
=====================================================================
'''

class Dendrometer:
    id = 0
    alarm1 = []
    alarm2 = []
    s = False
    m = False
    h = False
    a = False

def update_alarms ():

    dendrometers = []
    alarms = open ("../files/config.txt", 'r')

    line = alarms.readline()
    while (line):
        dendrometer = Dendrometer()
        dendrometer.id = line[:2]
        dendrometer.alarm1 = [int(line[2:4]),int(line[4:6]),int(line[6:8])]
        dendrometer.alarm2 = [int(line[8:10]),int(line[10:12]),int(line[12:14])]
        dendrometer.s = (line[-5] == '0')
        dendrometer.m = (line[-4] == '0')
        dendrometer.h = (line[-3] == '0')
        dendrometer.a = (line[-2] == '0')

        dendrometers.append(dendrometer)

        line = alarms.readline()

    alarms.close()

'''
Second section: emulate the measures
====================================
'''

last_time = []
last = 10
date = [25,3,2018]


def measure (id, time):
    file = open ("../files/Mediciones_" + str(id) + ".txt", 'a')

    file.write(     "R=" + str(float(last+randint(10, 20))) + \
                    " L=" + str(float(last+randint(0, 5))) + \
                    " H=666 1918/02/09 12:10:33" + \
                    " RSSI= " + str(-1 * randint(50, 70)) + \
                    " " + str(time[0]) + ":" + str(time[1]) + ":" + str(time[2]) + " " + \
                    str (date[0]) + "/" + str(date[1]) + "/" + str(date[2]) + "\n" \
    )

    file.close()

'''
Third section: simulate a clock for the alarms
===============================================
'''

def update ():
    download_reset()
    # do whatever you need with the reset information

    download_config()
    update_alarms ()



def check (time):
    for dend in dendrometers:
        if (dend.a):

            if (time == dend.alarm1 or time == dend.alarm2):
                measure(dend.id, time)
                #print ("[Measure] id: " + str(dend.id) + "\tat time: " + str(time[0]) + ":" + str(time[1]) + ":" + str(time[2]))

            alert1 = [dend.alarm1[0],dend.alarm1[1]-5,dend.alarm1[2]]
            alert2 = [dend.alarm1[0],dend.alarm1[1]-5,dend.alarm1[2]]

            # 5 minutes before alarm, trigger update
            if (time == alert1 or time == alert2):
                print ("[Download] id: " + str(dend.id) + " at time: " + str(time[0]) + ":" + str(time[1]) + ":" + str(time[2]))


        if (dend.h or dend.m or dend.s):
            alarm_time = 0
            if (dend.h):
                alarm_time += dend.alarm1[0] * 3600
            if (dend.m):
                alarm_time += dend.alarm1[1] * 60
            if (dend.s):
                alarm_time += dend.alarm1[2]

            now_time = time[0] * 3600 + time[1] * 60 + time[2]

            alert_time = alarm_time - 300

            # 5 minutes before alarm, trigger update
            if (alarm_time != 0 and (now_time + 300)%alarm_time==0):
                print ("[Download] id: " + str(dend.id) + " at time: " + str(time[0]) + ":" + str(time[1]) + ":" + str(time[2]))


            if (alarm_time != 0 and now_time%alarm_time==0):
                measure(dend.id, time)
                #print ("[Meassure] id: " + str(dend.id) + "\tat time: " + str(time[0]) + ":" + str(time[1]) + ":" + str(time[2]))

for day in range (3):
for hour in range (24):
    for minute in range (60):
        for second in range (60):
            check([hour, minute, second])
            time.sleep(1)


'''
Fourth section: git publication
===============================
'''

os.system("cd ~/sequoia; openode deploy;")
