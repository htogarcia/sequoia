'''
Script to update config.txt and and reset.txt
@author: Humberto Barrantes
@date: Apr-2018
@dependencies:
    python-pip:     sudo apt-get install python-pip
    pymongo:        python -m pip install pymongo
'''

'''
imports
'''
from pymongo import MongoClient

client = MongoClient("mongodb://sequoia:root@ds147668.mlab.com:47668/sequoia")
db = client.sequoia

'''
[reset.txt]
Contains dendrometers' id that are going to be re-established.

Format: One id per line.
'''

def download_reset():
     cursor = db.reset.find()
     reset_text = ""
     for document in cursor:
         if (document['reset'] == '1'):
             reset_text += document['_id'] + "\n"
         print(' id: ' + document['_id'] + '\treset? ' + document['reset'])

     reset_file = open("../files/reset.txt", 'w')
     reset_file.write(reset_text)
     reset_file.close();


'''
[config.txt]
Contains the alarms of all the dendrometers.

Format:

ID-HHMMSS-HHMMSS-SMHA
10-193000-043000-0010

Dendrometer         id: 10
First alarm         07:30:00 pm
Second alarm        04:30:00 am
Consider seconds    false
Consider minutes    false
Consider hours      true
Consider alarms     false
'''

def download_config ():
    cursor = db.set.find()
    set_text = ""
    for document in cursor:
        set_text += document['_id'] + document['date_1'].replace(':','') + \
            document['date_2'].replace(':','') + document['enable_seconds'] + \
            document['enable_minuntes'] + document['enable_hour'] + document['enable_alarm'] + "\n"

        print ( "node " + str(document['_id']) + " up to date")

    set_file = open("../files/config.txt", 'w')
    set_file.write(set_text)
    set_file.close();

#download_reset();
#download_config();
