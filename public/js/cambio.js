/**
 * Checks that a measurement has been taken after the date indicated by the user
 * @param date date of the data
 * @return true if the measurement was taken after
 */
function afterStart (date) {
  if (document.getElementById('time1').value) {
    if (date >= new Date(document.getElementById('time1').value)) {
      return true;
    }
    else {
      return false;
    }
  }
  else {
    return true;
  }
}

/**
 * Checks that a measure has been taken before the date indicated by the user
 * @param date date of the data
 * @return true if the measurement was taken before
 */
function beforeEnd (date) {
  if (document.getElementById('time2').value) {
    if (date <= new Date(document.getElementById('time2').value)) {
      return true;
    }
    else {
      return false;
    }
  }
  else {
    return true;
  }
}

/* this variables store all the data measured by the dendrometer, in case the
user wants to filter the data */
var DataA = new Array();
var DataB = new Array();
var DataC = new Array();
//var DataD = new Array();

var Dates = new Array();
var allDots;

/**
 * Reads data files and loads the measures in global arrays
 */
function loadData () {
  var node_file = "files/Mediciones_" + localStorage.getItem("node") + ".txt";
  $.ajax({
    url:node_file,
    success: function (data){

      /* 1. Extract the data from file */
      var Regex = /R=(.*) L=([^\s]*)(.)*RSSI=.* (.*)\/(.*)\/(.*)/g;
      var match = Regex.exec(data);
      while (match != null) {
        var dataA = parseFloat(match[2]);
        var dataB = parseFloat(match[1]);
        var dataC = parseFloat(((dataA+dataB)/2).toFixed(2));
        //dataD = dataC - ((dataA+dataB)/2);

        // new Date (year, month, day) NOTE: months starts in 0
        var date = new Date(
          parseInt(match[6]),
          parseInt(match[5])-1,
          parseInt(match[4])
        );

        DataA[DataA.length] = dataA;
        DataB[DataB.length] = dataB;
        DataC[DataC.length] = dataC;
        //DataD[DataD.lenght] = dataD;
        Dates[Dates.length] = date;

        match = Regex.exec(data);
      }
      setDateInputs(Dates);
      createGraph ();
    }
  });
}

function setDateInputs (Dates) {

  var inicio = getYYYYDDMM(Dates[0]);
  document.getElementById('inicio').innerText = inicio;
  document.getElementById('time1').value = inicio;
  document.getElementById('time1').min = inicio;
  document.getElementById('time1').def

  var fin = getYYYYDDMM(Dates[Dates.length-1]);
  document.getElementById('fin').innerText = fin;
  document.getElementById('time2').value = fin;
  document.getElementById('time2').max = fin;

  document.getElementById('time2').min = inicio;
  document.getElementById('time1').max = fin;
}

/**
 * Converts a date type object to year/day/month format
 * @param date_obj date type object to convert
 */
function getYYYYDDMM (date_obj) {
  var year = date_obj.getFullYear();
  var month = (date_obj.getMonth()+1);
  var day = date_obj.getDate();
  return year + '-' + (month<10?'0'+month:month) + '-' + (day<10?'0'+day:day);
}

/**
 * Updates the graph to display all the data or distributed measures along time
 */
function createGraph () {

  var temporal_dataA = [];
  var temporal_DataB = [];
  var temporal_dataC = [];
  //var temporal_dataD = [];
  var temporal_dates = [];

  for (var index = 0; index <DataA.length; index++) {
    if (afterStart(Dates[index]) && beforeEnd(Dates[index])) {
      temporal_dataA[temporal_dataA.length] = DataA[index];
      temporal_DataB[temporal_DataB.length] = DataB[index];
      temporal_dataC[temporal_dataC.length] = DataC[index];
      //temporal_dataD[temporal_dataD.length] = DataD[index];
      temporal_dates[temporal_dates.length] = Dates[index];
    }
  }

  var number_of_dots = 40;
  if (window.innerWidth < 500){
    number_of_dots = 10;
  }

  // plot all the data
  if (allDots || temporal_dataA.length < number_of_dots) {
    plot (temporal_dataA, temporal_DataB, temporal_dataC, temporal_dates);
    //plot (temporal_dataA, temporal_DataB, temporal_dataC, temporal_dataD, temporal_dates);
  }
  // plot a maximum of 50 meassures
  else {
    var temporal_dataA_2 = [];
    var temporal_DataB_2 = [];
    var temporal_dataC_2 = [];
    var temporal_dates_2 = [];

    for (var step = 0; step<temporal_dataA.length; step=step+parseInt(temporal_dataA.length/number_of_dots)) {
      temporal_dataA_2.push(temporal_dataA[step]);
      temporal_DataB_2.push(temporal_DataB[step]);
      temporal_dates_2.push(temporal_dates[step]);
      temporal_dataC_2.push(temporal_dataC[step]);
      //temporal_dataD_2.push(temporal_dataC[step]);
    }
    plot (temporal_dataA_2, temporal_DataB_2, temporal_dataC_2, temporal_dates_2);
    //plot (temporal_dataA_2, temporal_DataB_2, temporal_dataC_2, temporal_dataD_2, temporal_dates_2);
  }
}

/* Stores the chart to be able of change it  */
var chart;

/**
 * Plots the data given in the parameters, filtering it just by the date inputs.
 * @param dataA measure made by the dataA sensor of the dendrometer
 * @param dataB measure made by the dataB sensor of the dendrometer
 * @param dataC dataC between dataA and dataB
 * @param dates date when each measure was taken
 */
function plot (dataA, dataB, dataC, dates) {
//function plot (dataA, dataB, dataC, dataD, dates) {

  if (chart != undefined) {chart.destroy(); }

  chart = new Chart(document.getElementById("line-chart"), {
  type: 'line',
  data: {
    labels: dates.map(
      function (item) {
        return getYYYYDDMM(item);
      }
    ),
    datasets: [{
        data: dataA,
        label: "Sensor Izquierdo",
        borderColor: document.getElementById("colorL").value,
        pointRadius: allDots ? 0 : 4,
        fill: false
      }, {
        data: dataB,
        label: "Sensor Derecho",
        borderColor: document.getElementById("colorR").value,
        pointRadius: allDots ? 0 : 4,
        fill: false
      }, {
        data: dataC,
        label: "Promedio",
        borderColor: document.getElementById("colorA").value,
        pointRadius: allDots ? 0 : 4,
        fill: false
      }/*, {
        data: dataD,
        label: "Data C name",
        borderColor: #ff00ff,
        pointRadius: allDots ? 0 : 4,
        fill: false
      }*/
    ]
  },
  options: {
    title: {
      display: true,
      text: 'Cambio de grosor a lo largo del tiempo'
    }, scales: {
      yAxes: [{
        scaleLabel: {
          display: true,
          labelString: "Grosor del tronco (cm)"
        },
        ticks: {
          display: (window.innerWidth<500 ? false : true)
        }
      }],
      xAxes: [{
        display: !allDots,
        scaleLabel: {
          display: true,
          labelString: "Fecha (año-mes-dia)"
        },
        ticks: {
          display: (window.innerWidth<500 ? false : true)
        }
      }]
    }
  }
  });
}

/**
 * Changes the color of the lines in the graph. Triggered by a color input change
 */
function colorChange (e) {
  chart.data.datasets[0].borderColor = document.getElementById("colorL").value;
  chart.data.datasets[1].borderColor = document.getElementById("colorR").value;
  chart.data.datasets[2].borderColor = document.getElementById("colorA").value;
  chart.update();
}

/**
 * Updates chart when any date input changes
 */
function dateChange (e) {
  if (!document.getElementById('time1').value) {
    document.getElementById('time1').value = document.getElementById('time1').min;
  }
  if (!document.getElementById('time2').value) {
    document.getElementById('time2').value = document.getElementById('time2').max;
  }
  createGraph ();
}

/**
 * Plots all points when user wants
 */
function allPoints () {
  allDots = (document.getElementById("allPoints").checked) ? true : false;
  createGraph ();
}

function nodeChange() {
  var next_node = document.getElementById("node_name").value;
  localStorage.setItem("node", next_node);
  location.reload();
}

/**
 * Create chart including all the measures made over time
 */
window.onload = function (){
  flag = false;
  loadData();

  /* load names in combobox */
  var node = localStorage.getItem("node");

  var cont = 0;
  var name = localStorage[cont.toString()];
  while (name != null) {
    if (node == name) {
      document.getElementById("node_name").innerHTML += "<option selected>" + name + "</option>";
    }
    else {
      document.getElementById("node_name").innerHTML += "<option>" + name + "</option>";
    }
    name = localStorage[(++cont).toString()]
  }
}

window.onresize = function (event) {
  createGraph ();
};
