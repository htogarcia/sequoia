/* nodes and edges MUST BE GLOBAL */

/* DataSet to contain the nodes */
var nodes = new vis.DataSet();
nodes.add({id: 0, image: "images/raspberry2.png"});
var checked = false;

/* DataSet to contain the edges */
var edges = new vis.DataSet([{}]);

var popupMenu = undefined;

/**
 * Shows the node options menu when a node is clicked
 * @param properties information of the click event, including node clicked id
 */
function onClick (properties, container) {

  if (popupMenu !== undefined) {
    popupMenu.parentNode.removeChild(popupMenu);
    popupMenu = undefined;
  }

  if (properties.nodes.length == 1 ) {
    var node_clicked = nodes.get(properties.nodes[0]);
    if (node_clicked.id !== 0) {

      console.log(properties.pointer);

      var offsetLeft = container.offsetLeft;
      var offsetTop = container.offsetTop;

      popupMenu = document.createElement("div");
      popupMenu.className = 'col-lg-4 col-md-6 col-sm-6';
      popupMenu.style.left = properties.pointer.DOM.x - offsetLeft + 'px';
      popupMenu.style.top = properties.pointer.DOM.y - offsetTop +'px';
      popupMenu.style.position = 'absolute';

      var popupHTML = `
        <div class="bs-component">
          <div class="list-group">
            <a class="list-group-item list-group-item-action active">[NODE]</a>
            <a href="/cambio" class="list-group-item list-group-item-action" title="Crea un gráfico utilizando las medidas de cambio de grosor tomadas por el dendrómetro">Ver Gráfico Cambio de Grosor</a>
            <a id="control" href="/alarmas" class="list-group-item list-group-item-action" title="Permite establecer nuevas alarmas al dendrómetro">Nueva Alarma/Reestablecer</a>
          </div>
        </div>`;
      popupMenu.innerHTML = popupHTML.replace("[NODE]", "nodo " + node_clicked._name);
      container.appendChild(popupMenu);

      localStorage.setItem("node", node_clicked._name);
      localStorage.setItem("node_id", node_clicked.id);
    }
  }
  else {
    document.getElementById('options').style.visibility = "hidden";
  }

}

/**
 * Shows node Label and last message information when mouse hovers over a node
 * @param properties node id and canvas info when mouse hovers a node
 */
function onHoverNode (properties) {

  var node_hovered = nodes.get(properties.node);

  nodes.update({
    id:properties.node,
    label:node_hovered._label
  });

  /* checks dendrometers when user hovers a node */
  if (!checked) { check_dendrometers(); }

}

/**
 * Hide node Label and last connection when mouse blurs a node
 * @param properties node id and canvas info when mouse leaves a node
 */
function onBlurNode (properties) {

  var node_hovered = nodes.get(properties.node);

  nodes.update({
    id:properties.node,
    label:""
  });

}


/**
 * Creates the network using the data from the txt files in the server,
 * also asociate the functions above with the actions of the network.
 * @param nodes array of nodes including: name, RSSI and lastupdate.
 */
function createNetwork (nodes, edges) {

  console.log(nodes.length);

  /* 1. Get the tag where the network will be placed */
  var container = document.getElementById('mynetwork');

  /* 2. Create Data variable with the existents nodes and edges */
  var data = { nodes: nodes, edges: edges };

  /* 3. Configuration of the network */
  var options = {
    edges:{
      font: {
        align: 'middle'
      },
      smooth: {
        enabled: true,
        type: "dynamic"
      },
      length: 150,
      width: 2
    },
    nodes:{
      shape: "image"
    },
    interaction:{
      hover: true
    }
  };

  /* 4. Initialize your network! */
  var network = new vis.Network(container, data, options);

  /* 5. configure network events */
  network.on( 'click', function (properties) { onClick(properties, container); });

  network.on ( 'hoverNode', function(properties) { onHoverNode(properties); });

  network.on ( 'blurNode', function(properties) { onBlurNode(properties); });

}


function check_dendrometers () {

  checked = true;

  $.ajax({
    url:('files/config.txt'),
    success: function (data){

      var alarms = [];
      var today = new Date(
        new Date().getFullYear(),
        new Date().getMonth() + 1,
        new Date().getDate(),
        new Date().getHours(),
        new Date().getMinutes(),
        new Date().getSeconds(),
        0
      );

      var lines = data.split('\n');
      for (var line=0; line < lines.length && lines[line]!=""; line++) {

        var id = lines[line].substring(0,2);
        var alarm1 = new Date(
          new Date().getFullYear(),
          new Date().getMonth() + 1,
          new Date().getDate(),
          parseInt(lines[line].substring(2,4)),
          parseInt(lines[line].substring(4,6)),
          parseInt(lines[line].substring(6,8)),
          0
        );

        var alarm2 = new Date (
          new Date().getFullYear(),
          new Date().getMonth() + 1,
          new Date().getDate(),
          parseInt(lines[line].substring(8,10)),
          parseInt(lines[line].substring(10,12)),
          parseInt(lines[line].substring(12,14)),
          0
        );

        var s = (lines[line].substring(14,15) == '0')
        var m = (lines[line].substring(15,16) == '0')
        var h = (lines[line].substring(16,17) == '0')
        var a = (lines[line].substring(17,18) == '0')

        /* Compare each line from config file with las update of each node */

        nodes.forEach(function (node) {

          if (node._name == id && node._last != undefined) {

            var sRegex = /([0-9]+):([0-9]+):([0-9]+)\s([0-9]+)\/([0-9]+)\/([0-9]+)/g;
            var match = sRegex.exec(node._last);

            var last_update = new Date (parseInt(match[6]),parseInt(match[5]),parseInt(match[4]),
              parseInt(match[1]),parseInt(match[2]),parseInt(match[3]),0);

            if (a) {
              if (alarm1 < today && last_update < alarm1) {
                alert_notification(node._name, last_update);
              }
              if (alarm2 < today && last_update < alarm2) {
                alert_notification(node._name, last_update);
              }
            }
            else {

              var next_update = new Date (
                last_update.getFullYear(),
                last_update.getMonth(),
                last_update.getDate(),
                last_update.getHours() + ( (h)?alarm1.getHours():0 ),
                last_update.getMinutes() + ( (m)?alarm1.getMinutes():0 ),
                last_update.getSeconds() + ( (s)?alarm1.getSeconds():0 ),
                0
              );

              if (next_update < today && last_update < next_update) {
                alert_notification(node._name, last_update);
              }
            }
          }
        });
      }
    }
  });
}

function alert_notification (node, date) {
  document.getElementById("alert_div").style.display = "block";
  var alert_message = document.getElementById("alert_message");
  alert_message.innerText += "El nodo [NODO] no ha enviado ningún mensaje desde: [FECHA]";
  alert_message.innerText = alert_message.innerText.replace("[NODO]", node);
  alert_message.innerText = alert_message.innerText.replace("[FECHA]",date);
}

/**
 * Reads dendrometers txt files to extract information about each dendrometer,
 * and create the nodes and edges Datasets that will be used in the function
 * createNetwork.
 */
 window.onload = function (){
   var count = 0;
   document.getElementById("alert_div").style.display = "none";
   /* 1. dendrometros endpoint retrieve all up-to-date measures files */
   $.ajax({
    url: '/dendrometros',
    success: function(json){
      $.each(json, function (i, item) {
        $.ajax({
          url:('files/' + item.name),
          success: function (data){

            localStorage[(count++).toString()] = item.dendrometer;

            /* 2. Extract last message information using regular expression */
            var lines = data.split("\n");
            var sRegex = /RSSI= (.*) (\d+:\d+:\d+\s\d+\/\d+\/(\d)+)/g;
            var last_update = lines[lines.length-2];
            var match = sRegex.exec(last_update);

            /* 3. Create a dendrometer object with the information in the file */
            var dendrometer = {
              id:count,
              name:item.dendrometer,
              RSSI: match[1],
              date_time: match[2]
            };

            /* 4. Create new Node */
            nodes.add ({
              id: dendrometer.id,
              image: "images/tree2.png",
              _label: (dendrometer.name + '\n' + dendrometer.date_time),
              _last: dendrometer.date_time,
              _name: dendrometer.name
            });

            /* 5. Create new Edge */
            edges.add ({
              from: dendrometer.id,
              to: 0,
              label: ('RSSI='+dendrometer.RSSI)
            });

          }
        });
      });
    }
  });

  createNetwork(nodes, edges);
}
