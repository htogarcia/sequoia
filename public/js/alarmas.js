/**
 * Writes the name of the node we are modifying
 */
window.onload = function (){

  /* load names in combobox */
  var node = localStorage.getItem("node");

  var cont = 0;
  var name = localStorage[cont.toString()];
  while (name != null) {
    if (node == name) {
      document.getElementById("node_name").innerHTML += "<option selected>" + name + "</option>";
    }
    else {
      document.getElementById("node_name").innerHTML += "<option>" + name + "</option>";
    }
    name = localStorage[(++cont).toString()]
  }

  document.getElementById('interval').style.display = "none";
  document.getElementById('alarms').style.display = "block";

}

function time_unit () {
  var time_unit = (document.getElementById("timeunit").checked) ? true : false;

  document.getElementById('interval').style.display = time_unit ? "block" : "none";
  document.getElementById('alarms').style.display = !time_unit ? "block" : "none";
}

/**
 * Sends a POST request to the server, asking to reset the node we are modifying
 */
function post_reset (){
  $.post(
    "/reset",
    { _id: localStorage.getItem("node"), reset: 1 },
    function(data, status){
      if (status == "success") {
        document.getElementById('success-btn').click();
      }
      else {
        document.getElementById('danger-btn').click();
      }
      console.log(status);
    });
};

/**
 * Sends a POST request to the server, asking to reprogram the node we are
 * modifying. Take the new alarm from the minutes input.
 */
function post_set_min (){
  var minutes = document.getElementById('minutes').value;
  var hours = Math.floor (minutes/60);
  var date = (minutes > 59) ?
    hours + ":" + (minutes-(hours*60)) + ":0" :
    "0" + ":" + minutes + ":0";
  $.post(
    "/set",
    {
      _id: localStorage.getItem("node"),
      date_1: date,
      date_2: "0:0:0",
      enable_seconds: 1,
      enable_minuntes: 0,
      enable_hour: hours != 0 ? 0 : 1,
      enable_alarm: 1
    },
    function(data, status){
      if (status == "success") {
        document.getElementById('success-btn').click();
      }
      else {
        document.getElementById('danger-btn').click();
      }
      console.log(status);
    });
};

function nodeChange() {
  var next_node = document.getElementById("node_name").value;
  localStorage.setItem("node", next_node);
  location.reload();
}

/**
 * Sends a POST request to the server, asking to reprogram the node we are
 * modifying. Takes the new alarm from the date inputs.
 */
function post_alarms (){
  var alarm1 = document.getElementById('time1').value;
  var alarm2 = document.getElementById('time2').value;


  /* manage bug inside input (type=date) */
  if (alarm1.split(":").length-1 == 1) {
      console.log(alarm1);
      alarm1 += ":00";
  }
  if (alarm2.split(":").length-1 == 1) {
      console.log(alarm2);
      alarm2 += ":00";
  }

  var enable_hours = document.getElementById('enable_hours').checked ? 0 : 1;
  var enable_minutes = document.getElementById('enable_minutes').checked ? 0: 1;
  var enable_seconds = document.getElementById('enable_seconds').checked ? 0 : 1;

  var enable = (enable_hours + enable_minutes + enable_seconds) == 3 ? 0 : 1;

  $.post(
    "/set",
    {
      _id: localStorage.getItem("node"),
      date_1: alarm1 != "" ? alarm1 : "0:0:0",
      date_2: alarm2 != "" ? alarm2 : "0:0:0",
      enable_seconds: enable_seconds,
      enable_minuntes: enable_minutes,
      enable_hour: enable_hours,
      enable_alarm: enable
    },
    function(data, status){
      if (status == "success") {
        document.getElementById('success-btn').click();
      }
      else {
        document.getElementById('danger-btn').click();
      }
      console.log(status);
    });
};
